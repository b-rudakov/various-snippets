<?php

/**
 * Enables correct routing like:
 * /shop                    - Shop URL
 * /shop/phohes             - Category page URL
 * /shop/phones/iphone-12   - Product page URL
 * 
 * To make it work set your WP permalinks (/wp-admin/options-permalink.php) as follows:
 * Product category base    = shop
 * Product permalinks       = Shop base with category
 * 
 * This action fixes breadcrumbs generation
 * However "Category page URL" will become unavailable, it shows 404
 * The code bellow returs this page back (～￣▽￣)～
 */

add_action('created_product_cat', 'rewrite_for_each_category', 10, 0);
add_action('saved_product_cat', 'rewrite_for_each_category', 10, 0);
function rewrite_for_each_category( $flash = false ) {

    // get shop base
    $permalink = get_option('woocommerce_permalinks')['category_base'];

    global /** @var WP_Rewrite $wp_rewrite */
    $wp_rewrite;

    // get all product categories
    $terms = get_terms( [
        'taxonomy' => 'product_cat',
        'post_type' => 'product',
        'hide_empty' => false,
    ]);

    // for each category we create rewrite rule
    /** @var WP_Term $term */
    foreach ( $terms as $term ) {
        add_rewrite_rule( "$permalink/$term->slug/" . '?$', 'index.php?post_type=product&product_cat=' . $term->slug, 'top' );
        add_rewrite_rule( "$permalink/$term->slug/" . 'page/([0-9]{1,})/?$', 'index.php?product_cat=' . $term->slug . '&paged=$matches[1]', 'top' );
        add_rewrite_rule( "$permalink/$term->slug/" . '(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?product_cat=' . $term->slug . '&feed=$matches[1]', 'top' );
    }

    // I'm not sure why these two lines are necessary, but without it... nothing works
    $wp_rewrite->wp_rewrite_rules();
    flush_rewrite_rules( false );
}
