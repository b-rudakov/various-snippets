<?php

/**
 * Disables "Back to WordPress Editor" if Elementor editor already in use
 */

add_action('admin_head', fn() => print_r("
<style>
	body.elementor-editor-active #elementor-switch-mode {
		display: none !important;
		visibility: hidden !important;
	}
</style>
"));
